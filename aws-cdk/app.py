#!/usr/bin/env python3

# For consistency with TypeScript code, `cdk` is the preferred import name for
# the CDK's core module.  The following line also imports it as `core` for use
# with examples from the CDK Developer's Guide, which are in the process of
# being updated to use `cdk`.  You may delete this import if you don't need it.
from aws_cdk import core

from environment.application import ApplicationStack
import environment.sharedVars as sv

app = core.App()

# get the list of shared variables...
shared_vars = sv.SharedVars(app)
if app.node.try_get_context("echoVars"):
    shared_vars.echo_vars()

env=core.Environment(account=shared_vars.account, region=shared_vars.region)

s = ApplicationStack(app, shared_vars.app_stack_name,env=env)
core.Tags.of(s).add('STACK_NAME',shared_vars.app_stack_name)

app.synth()