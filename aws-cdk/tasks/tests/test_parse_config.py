import sys, os
import yaml

sys.path.insert(1, os.path.abspath(sys.path[0]+"/../code"))

from parse_config import *

conf = yaml.safe_load("""
redactChar: '#'
replaceStr: 'Lorem Ipsum'

patterns:
  email: '.*@*\.com'
  ss: '[0-9]{3}-[0-9]{2}-[0-9]{4}'
  cc: '[0-9]{4}'

# if a column is specified with no other information its value will be hashed
tables:
  userbase:
    email:
      redact:
      hash:
        - cc
    firstName:
    lastName:
    notes:
    phone:
    userName:""")

tmp_list = config_to_sql(conf)
assert tmp_list != None
assert len(tmp_list) == 1

print(tmp_list)


