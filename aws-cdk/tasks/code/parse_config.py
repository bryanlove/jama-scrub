import collections
import sys

action_map = {'remove': 'REGEXP_REPLACE',
             'redact': 'redact',
             'replace': 'REGEXP_REPLACE',
             'hash': 'redact'}

str_replace_map = {
    'remove':'',
    'replace':None,
    'redact':None,
    'hash': None
}


def config_to_sql(config_map):
    if 'enabled' in config_map and not config_map['enabled']:
        return None

    # set the values for str_replace_map from the config_map (if they exist)
    if 'replaceStr' in config_map:
        str_replace_map['replace'] = config_map['replaceStr']
    if 'redactChar' in config_map:
        str_replace_map['redact'] = config_map['redactChar']

    # convert the configuration to SQL statements...
    all_sql = []

    for (tbl,cols) in config_map['tables'].items():
        sql = 'update '+tbl+' set '
        sets = []
        for col, actions in cols.items():
            if actions == None:
                sets.append(col+' = MD5('+col+')')
            else:
                sets.append(col+' = '+get_sql_for_col(config_map, col, actions))

        all_sql.append(sql+','.join(sets)+';')
    
    return all_sql


# create the SQL statement for a given column...
def get_sql_for_col(config_map, col, actions):
    '''Converts all of a columns actions and patterns into SQL, creating as few statements as possible.'''

    action_str = col

    # ENFORCE THE RULES!
    # NOTE: I moved the config validation routine to the lambda because it's more useful there. This Task code should only ever receive validated config from the Lambda
        
    # loop over actions and patterns....
    for action, patterns in actions.items():
        func_name = action_map[action]
        repl_str = "'"+str_replace_map[action]+"'" if str_replace_map[action] != None else 'NULL'

        # no patterns? we're just removing the whole value - set the value to an empty string (not a null)
        if patterns is None:

            if action == 'redact':
                # if redacting a whole column, remove letters and numbers and leave symbols. This way email address will still be in a valid format
                action_str = "regexp_replace("+col+",'[0-9A-z]','"+repl_str+"')"
            elif action == 'hash':
                action_str = "md5("+col+")"
            else:
                action_str = "'"+repl_str+"'"

        # loop over the patterns, nesting each replace inside the next one until we've processed all the actions and patterns for this column
        else:
            for pattern in patterns:
                this_regex = config_map['patterns'][pattern]
                # Notice there's no quotes around repl_str... this allows us to pass a NULL to the SQL function for redacting with hash values
                action_str = func_name+"("+action_str+",'"+this_regex+"',"+repl_str+")"
    
    # I now have all the actions and patterns for this column combined into one statement...
    return action_str
