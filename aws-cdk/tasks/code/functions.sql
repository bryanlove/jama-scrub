SET GLOBAL log_bin_trust_function_creators = 1;

DELIMITER $$
CREATE FUNCTION redact(src_str varchar(65000), regex varchar(65000), redact_char char(1))
RETURNS varchar(65000)
BEGIN
  DECLARE vmatch varchar(65000);
  DECLARE replacement varchar(65000);

  SET vmatch = REGEXP_SUBSTR(src_str, regex);
  
  WHILE (vmatch is not null)
  DO
    IF redact_char IS NULL THEN
        SET replacement = md5(vmatch);
    ELSE
        SET replacement = regexp_replace(vmatch,'[0-9A-z]',redact_char);
    END IF;
    SET src_str = REPLACE(src_str, vmatch, replacement);
    SET vmatch = REGEXP_SUBSTR(src_str, regex);
  END WHILE;

  RETURN src_str;
END$$
DELIMITER ;
