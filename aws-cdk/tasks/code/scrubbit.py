import boto3
import json
#import mysql.connector
#import botocore as bc
import sys
import os
from parse_config import *

keep_db = True

############################ FUNCTIONS ############################
def exec_file(fname):
    command = """mysql -u %s -p"%s" --host %s --port %s %s < %s""" %('root', 'jamaroot', mhost, '3306', db_name, fname)
    os.system(command)

############################ BODY ############################

print("starting...")

#using RDS might be overkill... could just spin up a local mysql db instead
instanceIdentifier = 'blove-test-rds'
db_name = 'scrubber'
s3_source_bucket = os.environ["s3_source_bucket"]
s3_file = os.environ["s3_file"]
s3_dest_bucket = os.environ["s3_destination_bucket"]
dest_file_name = 'clean_'+s3_file
scrub_config = os.environ["scrub_config"]

print(scrub_config)
scrub_config = json.loads(scrub_config)


client = boto3.client('rds')

try:
    response = client.create_db_instance(
        DBName=db_name,
        DBInstanceIdentifier=instanceIdentifier,
        AllocatedStorage=20,
        DBInstanceClass='db.m6g.large',
        Engine='mysql',
        MasterUsername='root',
        MasterUserPassword='jamaroot',
        MultiAZ=False,
        BackupRetentionPeriod=0)

except client.exceptions.DBInstanceAlreadyExistsFault:
    print("DB Already exists:", sys.exc_info()[0])
    #raise

# Waiter - wait until the instance is ready
waiter = client.get_waiter('db_instance_available')
waiter.wait(
    DBInstanceIdentifier=instanceIdentifier
)

# get hostname of instance
response = client.describe_db_instances(
    DBInstanceIdentifier=instanceIdentifier
)
mhost = response['DBInstances'][0]['Endpoint']['Address']

# get backup file
print("Downloading file: "+s3_source_bucket+"/"+s3_file)
boto3.client('s3').download_file(s3_source_bucket, s3_file, 'tmp.sql')

# restore backup file
print("Restoring file")
exec_file('tmp.sql')
print("Done with restore")

# create the REDACT function in the DB
print("Creating REDACT function")
exec_file('functions.sql')

# generate the SQL we'll need to run
print("Converting config to SQL")
sql_statements = config_to_sql(scrub_config)

# write queries to a file
with open('cmds.sql', 'w') as fout:
    print('\n'.join(sql_statements), file=fout)
os.system('cat cmds.sql')

# execute the file
print("Executing SQL commands")
exec_file('cmds.sql')

# dump the database to file
print("Creating dump file")
command = """mysqldump --single-transaction --skip-lock-tables -u %s -p"%s" --host %s --port %s %s > %s""" %('root', 'jamaroot', mhost, '3306', db_name, dest_file_name)
os.system(command)

# move file to S3
print("Uploading file: "+s3_dest_bucket+"/"+dest_file_name)
boto3.client('s3').upload_file(dest_file_name, s3_dest_bucket, dest_file_name)

# destroy DB if necessary
if not keep_db:
    print("Destroying database")
    response = client.delete_db_instance(
        DBInstanceIdentifier=instanceIdentifier,
        DeleteAutomatedBackups=True,
        SkipFinalSnapshot=True)
