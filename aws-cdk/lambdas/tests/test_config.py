import sys
import os
import json

# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, os.path.abspath(sys.path[0]+"/../code"))

os.environ["CONFIG_DEFAULT_FILE"] = sys.path[0]+"/config/scrub_config.yml"
os.environ["CONFIG_CUSTOM_FILE"] = sys.path[0]+"/config/client_custom.yml"

from config_functions import *


########### test config validation ###########
from validation_test_values import *
for prefix,cc in TEST_CASES.items():
  try:
    msg = validate_config(cc)
  except Exception as e:
    assert str(e).startswith(prefix)


# get default configuration
cc = get_default_config()
# set all or part of the default config
print("asdf")

# get effective config for a single client
cc = get_client_config('c123')
#print(cc)
assert cc != None
assert 'enabled' in cc and cc['enabled'] == True
assert 'configFile' in cc
assert 'config' in cc

cc = get_client_config('c345')
#print(cc)
assert cc != None
assert 'enabled' in cc and cc['enabled'] == False
assert 'configFile' in cc
assert 'config' in cc

cc = get_client_config('defaultClient')
#print(cc)
assert cc != None
assert 'enabled' in cc and cc['enabled'] == True
assert 'configFile' in cc
assert 'config' in cc

# set custom config file or enable/disable for a single client
print("asdf")



