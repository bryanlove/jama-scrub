TEST_CASES = {
    # --------- invalid string
    "Failed to":
    """
    ]tables{
    """,

    # --------- missing required fields
    "Required fields missing":
    """
    tables:
    """,

    # --------- must specify at least one table
    "Must specify":
    """
    redactChar: '#'
    replaceStr: 'Lorem Ipsum'

    patterns:
        email: '.*@*\.com'
        ss: '[0-9]{3}-[0-9]{2}-[0-9]{4}'
    tables:
    """,

    # --------- tables must have at least one column
    "Each table must have":
    """
    redactChar: '#'
    replaceStr: 'Lorem Ipsum'

    patterns:
        email: '.*@*\.com'
        ss: '[0-9]{3}-[0-9]{2}-[0-9]{4}'
    tables:
        mytable:
    """,

    # --------- unknown action
    "Unknown actions":
    """
    redactChar: '#'
    replaceStr: 'Lorem Ipsum'

    patterns:
        email: '.*@*\.com'
        ss: '[0-9]{3}-[0-9]{2}-[0-9]{4}'
    tables:
        mytable:
            mycol:
                asdf:
    """,

    # --------- pattern can only be used once per column
    "A pattern can only be used once":
    """
    redactChar: '#'
    replaceStr: 'Lorem Ipsum'

    patterns:
        email: '.*@*\.com'
        ss: '[0-9]{3}-[0-9]{2}-[0-9]{4}'
    tables:
        mytable:
            mycol:
                remove:
                    - ss
                redact:
                    - ss
    """
}