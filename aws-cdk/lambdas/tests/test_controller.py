import sys
import os
import json

# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, os.path.abspath(sys.path[0]+"/../code"))
os.environ["CONFIG_DEFAULT_FILE"] = sys.path[0]+"/config/scrub_config.yml"
os.environ["CONFIG_CUSTOM_FILE"] = sys.path[0]+"/config/client_custom.yml"


from controller import handler

# test bad function call
foo = handler(json.loads('{"action":"bad_func","args":[1]}'), None)
# test missing action
foo = handler(json.loads('{"wtf":"bad_func","args":[1]}'), None)
# test wrong number of args
foo = handler(json.loads('{"action":"get_default_config","args":[]}'), None)
foo = handler(json.loads('{"action":"get_default_config","args":[1,2]}'), None)
foo = handler(json.loads('{"action":"get_default_config"}'), None)

# test good function calls
foo = handler(json.loads('{"action":"get_default_config","args":["c123"]}'), None)
exit
foo = handler(json.loads('{"action":"set_default_config","args":["some json"]}'), None)
foo = handler(json.loads('{"action":"get_client_config","args":["c123"]}'), None)
foo = handler(json.loads('{"action":"set_client_config","args":["c123","some json"]}'), None)




