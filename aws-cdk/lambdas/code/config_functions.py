import boto3
import json
import os
import yaml
import utility_functions as uf

config_bucket = os.environ.get("CONFIG_BUCKET",None)
config_default_file = os.environ["CONFIG_DEFAULT_FILE"]
config_custom_file = os.environ["CONFIG_CUSTOM_FILE"]

    

def get_default_config(format=None):
    # get default configuration
    config_map = uf.get_config_file_as_dict(config_bucket, config_default_file)
    
    if format == 'json':
        return json.dumps(config_map)
    elif format == 'yaml':
        return yaml.dump(config_map)
    else:
        return config_map


def get_custom_config(format=None):
    # get custom configuration
    config_map = uf.get_config_file_as_dict(config_bucket, config_custom_file)
    if format == 'json':
        return json.dumps(config_map)
    elif format == 'yaml':
        return yaml.dump(config_map)
    else:
        return config_map


def set_default_config(config_str):
    # set the default config
    # validation.. ensure that this new config is legit
    ofile = 'tmp_config_default'
    config_map = validate_config(config_str)
    with open(ofile, 'w') as file:
        yaml.dump(config_map, file)
    boto3.client('s3').upload_file(ofile, config_bucket, config_default_file)

    return 'Default config has been set'


# get effective config for a single client
def get_client_config(client_id, format=None):
    # every client starts with this...
    config_map = {'enabled':True, 'configFile':config_default_file}

    # get contents of custom config file to see if this client has an entry there...
    cust_map = uf.get_config_file_as_dict(config_bucket, config_custom_file)

    # do they have a custom config?
    if client_id in cust_map['clientIds']:
        # merge the declared config with custom config...
        # if client has a custom file, get that and merge it too
        cust_client_config = cust_map['clientIds'][client_id]
        config_map = (config_map | cust_map['defaults'])
        if cust_client_config != None:
            config_map = (config_map | cust_client_config)

    # now lets get the contents of the client's config file (either default or custom at this point)
    config_file = config_map['configFile']
    # ensure the file has a full path if it's a local file (not S3)
    if not config_file.startswith('/') and config_bucket == None:
        config_file = os.path.dirname(config_default_file)+"/"+config_file
    config_map['config'] = uf.get_config_file_as_dict(config_bucket, config_file)

    if format == 'json':
        return json.dumps(config_map)
    elif format == 'yaml':
        return yaml.dump(config_map)
    else:
        return config_map


def set_client_config(client_id, config_str):
    # set custom config file or enable/disable for a single client
    # validation.. ensure that this new config is legit
    new_config_map = validate_config(config_str)
    
    # get the custom config file contents
    client_custom = get_custom_config()

    cid_map = client_custom['clientIds']

    # create what the entry would be if it was a new entry, then lay existing values on top
    new_cust_entry = {'configFile': 'custom_config_'+str(client_id)+".yml"}

    # if they don't have an entry at all, create one where they are enabled
    existing_entry = cid_map.get(client_id, {'enabled': True})

    # if existing_entry is null then the client was defined with no value (disabled)...
    # ...otherwise, merge existing entry with new entry to get final entry
    if existing_entry != None:
        new_cust_entry += existing_entry

    # rewrite target file with requested config to S3
    fname = new_cust_entry['configFile']
    with open('/tmp/'+fname, 'w') as ofile:
        yaml.dump(new_config_map, ofile)
    boto3.client('s3').upload_file('/tmp/'+fname, config_bucket, fname)

    # rewrite the client_custom.yml file to S3
    client_custom['clientIds'][client_id] = new_cust_entry
    with open('/tmp/'+config_custom_file, 'w') as ofile:
        yaml.dump(client_custom, ofile)
    boto3.client('s3').upload_file('/tmp/'+config_custom_file, config_bucket, config_custom_file)

    # craft the return string
    msg = "Configuration saved in '"+config_bucket+"/"+new_cust_entry['configFile']+"'."
    if new_cust_entry.get('enabled', False):
        msg += " Client is currently DISABLED"

    return msg


# config can be a dictionary, a JSON string or a YAML string
# throws Exception if anything is wrong,
# returns dictionary if config is ok
def validate_config(config):
    # convert to dict if it isn't already...
    if type(config) is dict:
        config_map = config
    else:
        try:
            if config.startswith('{'):
                config_map = json.loads(config)
            else:
                config_map = yaml.safe_load(config)
        except Exception as e:
            raise Exception("Failed to parse string - structure invalid")

    # 1. required pieces exist
    diff_list = set(['redactChar','replaceStr','patterns','tables']) - set(config_map)
    if diff_list:
        raise Exception("Required fields missing: "+str(diff_list))

    # 2. must be at least one table defined
    if config_map['tables'] == None:
        raise Exception("Must specify at least one table")
    
    # 3. each table has at least one column
    bad_tables = [x for (x,y) in config_map['tables'].items() if y == None or len(y) == 0]
    if bad_tables:
        raise Exception("Each table must have at least one column defined: "+str(bad_tables))
    
    # 4. each table column can only be defined once
    ### NOTE: this test doesn't work... because it's a dictionary, we can't actually tell if a column was specified twice because the map just keeps the last defined value without complaining
    #bad_tables = [tbl for (tbl,cols) in config_map['tables'].items() if (len(cols) - len(set(cols)) > 0)]
    #if bad_tables:
    #    return "Columns must only be defined once per table: "+str(bad_tables)
    
    # 5. actions must be valid
    valid_actions = set(['remove','redact','replace','hash'])
    bad_actions = [tbl+"."+col+"."+"|".join(set(actions) - valid_actions) for (tbl,cols) in config_map['tables'].items() for col,actions in cols.items() if actions != None and (set(actions) - valid_actions)]
    if bad_actions:
        raise Exception("Unknown actions found: "+", ".join(bad_actions))

    # 6. only one action allowed if it has no value.. the others are pointless
    # total actions minus empty actions 
    action_map = {tbl+"."+col:actions for (tbl,cols) in config_map['tables'].items() for col,actions in cols.items()}

    for col,actions in action_map.items():
        empty_actions = [x for x,y in actions.items() if y == None]
        if len(empty_actions) > 0 and len(actions) > 1:
            raise Exception("Empty action ["+col+"."+empty_actions[0]+"] will negate effects of other actions if that action is blank")

    # 7. see if we have any duplicate patterns...
    for col,actions in action_map.items():
        pset = set()
        dupes = [pattern for patterns in actions.values() for pattern in patterns if pattern in pset or pset.add(pattern)]
        
        if dupes:
            raise Exception("A pattern can only be used once within a single column ["+col+"."+'|'.join(dupes)+"]")


    return config_map