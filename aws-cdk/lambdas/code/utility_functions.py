import os
import boto3
import botocore
import json
import yaml

task_family = os.environ.get("TASK_FAMILY",None)
container_name = os.environ.get("CONTAINER_NAME",None)
config_bucket = os.environ.get("CONFIG_BUCKET",None)


def lambda_success(msg):
    return str(msg)


def lambda_fail(msg):
    print("ERROR: "+str(msg))
    raise Exception(str(msg))


def get_config_file_as_dict(bucket, fname):
    if bucket == None: # used for local testing
        with open(fname, 'r') as file:
            config_str = file.read().strip() #.replace('\n', '').strip()
    else:
        try:
            fdata = boto3.client('s3').get_object(Bucket=bucket, Key=fname)
            config_str = fdata['Body'].read().decode('utf-8').strip()
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == "NoSuchKey":
                print("The object does not exist. Attempting local read and copy")
                # upload file so it'll be there next time... and return the contents of the file we just uploaded
                boto3.client('s3').upload_file('configs/'+fname, config_bucket, fname)
                return get_config_file_as_dict(None, 'configs/'+fname)
            else:
                raise

    config_map = json.loads(config_str) if config_str.startswith('{') else yaml.safe_load(config_str)

    return config_map


def run_task(overrides_dict):
    client = boto3.client('ecs')

    # build map in proper image
    param_list = [{'name': k,'value': v} for (k,v) in overrides_dict.items()]

    # https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ecs.html#ECS.Client.run_task
    response = client.run_task(taskDefinition=task_family,
                            cluster='arn:aws:ecs:us-west-2:247951156930:cluster/eng1-ecscluster-crz7l2mk',
                            launchType='FARGATE',
                            networkConfiguration={
                                    'awsvpcConfiguration': {
                                        'subnets': [
                                            'subnet-0b25ac3cf2a381ce3',
                                        ]
                                    }
                                },
                            overrides={
                                'containerOverrides': [
                                    {   'name': container_name,
                                        'environment': param_list,
                                    },
                                ]
                            }
    )

    return response