import boto3
import os
import json
import config_functions as cf
import utility_functions as uf

task_family = os.environ.get("TASK_FAMILY",None)
container_name = os.environ.get("CONTAINER_NAME",None)
source_bucket = os.environ.get("SOURCE_BUCKET",None)
destination_bucket = os.environ.get("DESTINATION_BUCKET",None)

def scrub_latest(client_id):
    # start a task in the background and return success/fail to indicate the task was or was not started.
    # will not start a task if the same task is already running (returns failure)
    client = boto3.client('ecs')
    
    # https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ecs.html#ECS.Client.run_task
    response = client.run_task(taskDefinition=task_family, 
                               overrides={
                                            'containerOverrides': [
                                                {
                                                    'environment': [
                                                        {'name': 'client_id', 'value': client_id},
                                                    ],
                                                },
                                            ]
                                        }
    )

    return response

def scrub_file(bucket, file):
    # start a task in the background and return success/fail to indicate the task was or was not started.
    # will not start a task if the same task is already running (returns failure)
    param_list = {
        's3_source_bucket': bucket,
        's3_file': file,
        's3_destination_bucket': destination_bucket,
        'scrub_config': json.dumps(cf.get_default_config())
    }

    #return TaskRunner(overrides_dict=param_list).response
    return uf.run_task(param_list)

######### OPTIONAL ##########
def kill_scrub(client_id):
     # kill any active scrubbing tasks for this client
    client = boto3.client('ecs')
    
    # https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ecs.html#ECS.Client.stop_task
    response = client.stop_task(
        cluster='string',
        task='string',
        reason='string'
    )
    return response


def get_active_scrubs():
    # list all running scrubber tasks and when they were started (and by whom?)
    client = boto3.client('ecs')

    # https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ecs.html#ECS.Client.list_tasks
    response = client.list_tasks(
        family=task_family,
        maxResults=200,
        #startedBy='string',
        #serviceName='string',
        desiredStatus='RUNNING',
        launchType='FARGATE'
    )
    return response


def get_last_scrub(client_id):
    # can get latest log entry for specified customer(s)
    print("asfd")


def get_active_logs(client_id):
    # can get log entries for running tasks
    print("asfd")


