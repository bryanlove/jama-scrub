import utility_functions as uf
from inspect import signature
from task_functions import *
from config_functions import *


# get a connection to the DB using the given secret

def handler(event, context):
    # figure out what we need to do
    # event is the request in JSON format, JSON will look like:
    #   {
    #       "action":"<function name>",
    #       "args":[<list of args>]
    #   }

    func_name = event.get('action',None)
    func_args = event.get('args',[])
    func_obj = globals().get(func_name,None)
    msg = None

    # validate input
    if func_name == None:
        msg = ["No action specified in payload"]
    elif func_obj == None:
        # requested function does not exist, send back a list of available functions
        #functions = {(func,fobj) for func,fobj in globals().items() if not func.startswith('__') and callable(fobj) and fobj.__module__.endswith('_functions')}
        functions = ['    '+func+'('+','.join(list(signature(fobj).parameters))+')' for func,fobj in globals().items() if not func.startswith('__') and callable(fobj) and fobj.__module__.endswith('_functions')]
        functions.sort()
        msg = ["Requested function does not exist. Callable functions are:"] + functions

    elif len(signature(func_obj).parameters) != len(func_args):
        arg_list = list(signature(func_obj).parameters)
        msg = ["received "+str(len(func_args))+" arg(s) - function '"+func_name+"' requires "+str(len(arg_list))+" arg(s): "+str(arg_list)]
    
    if msg != None:
        print('\n'.join(msg))
        uf.lambda_fail(str(msg))

    # call the requested function...
    try:
        msg = globals()[func_name](*func_args)
        return uf.lambda_success(str(msg))
    except Exception as e:
        uf.lambda_fail(str(e))
