import re
import textwrap
import sys
import os

########################################################################
# The purpose of this class is to create a single reference point
#  for all the variables used throughout this process.
#
# We can import this class into existing templates and any
#  new templates to ensure we're using the exact same values across
#  throughout the process.
#
# It also allows us to put a USAGE message and error checking in one
#  place.
########################################################################

class SharedVars():
    def __init__(self,app):


        # using 'self.<varname>' creates class variables that can be called using '<classname>.<varname>' later

        ### constants - information these processes need to know about... these are not created by the CDK
        self.s3_source_bucket = 'blove-test'
        self.s3_destination_bucket = 'blove-cleaned'

        ### variables for the CDK
        self.account = os.getenv('CDK_DEFAULT_ACCOUNT') or ''
        self.region = os.getenv('CDK_DEFAULT_REGION')
        self.app_env_name = "db-scrubber"
        self.app_stack_name = self.app_env_name+"-app"

        self.env_stack_name = 'vpc-bpsu4g4p' # the parent stack containing the VPC def

        self.app_s3_config_bucket_name = self.app_env_name+"-configs"
        self.app_config_default_file = "scrub_config.yml"
        self.app_config_custom_file = "client_custom.yml"

        self.app_lambda_role_name = self.app_env_name+"-lambda-role"
        self.app_lambda_name = self.app_env_name+"-lambda"

        self.app_task_role_name = self.app_env_name+"-task-role"
        self.app_task_name = self.app_env_name+"-task"
        self.app_task_container_name = "docker_db_scrubber"
        self.app_task_mem_limit = 1024 #cpu units are calculated as half this number

    # simply prints the key=value pairs from above to the screen
    def echo_vars(self):
        print("------------- stack values -------------")
        members = [attr for attr in dir(self) if not callable(getattr(self, attr)) and not attr.startswith("__") and type(getattr(self, attr)) != list]
        with open("../cdkVars", "w") as f:
            for v in members:
                print(v+"="+getattr(self,v))
                f.write("export "+v.upper()+"="+getattr(self,v)+"\n")
        print("----------------------------------------")
