from aws_cdk import (
    core as cdk,
    core as core,
    aws_s3 as s3,
    aws_lambda as _lambda,
    aws_iam as _iam,
    aws_ecs as ecs,
    aws_ecr as ecr
)
from aws_cdk.aws_ec2 import Vpc, SecurityGroup, SubnetSelection
from aws_cdk.aws_ecr import IRepository
from aws_cdk.aws_iam import Role
from aws_cdk.core import Fn
from aws_cdk.aws_s3 import BlockPublicAccess
import os
import shutil
import urllib.request
import environment.sharedVars as sv
import json


class ApplicationStack(cdk.Stack):
    # this is used everywhere and not modified, thus it's a global variable
    shared_vars = None

    def __init__(self, scope: cdk.Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)
        global shared_vars
        shared_vars = sv.SharedVars(scope)

        print("Creating - Stack:" + construct_id)

        core_obj_map = self.build_core_objects()
        s3_config_bucket = self.build_s3_buckets()
        self.build_lambda(core_obj_map, s3_config_bucket)
        self.build_task()


    def build_core_objects(self):
        print("Creating     - retrieving core objects")

        # have to get the parent VPC using tags because it's the only way that doesn't require knowing the actual VPC ID, which we don't have at this point in the process
        print("Creating     - retrieving VPC tagged with: "+shared_vars.env_stack_name)
        vpc = Vpc.from_lookup(self, "vpc", tags={'aws:cloudformation:stack-name' : shared_vars.env_stack_name})

        return {
            'obj_vpc':vpc
        }

    
    def create_dependency_pkg(self):
        # create a temp dir that has symlinks to the python dependencies we need to run the lambda
        shutil.rmtree('/tmp/lambda_python_libs/python',ignore_errors=True)
        os.makedirs('/tmp/lambda_python_libs/python')
        os.symlink(os.getcwd()+'/../.venv/lib/python3.9/site-packages/boto3', '/tmp/lambda_python_libs/python/boto3')

        # we also need the psycopg2 lib, which we have to download from github as a zip...
        if not os.path.isdir('/tmp/psycopg2/awslambda-psycopg2-master/psycopg2-3.8/'):
            urllib.request.urlretrieve('https://github.com/jkehler/awslambda-psycopg2/archive/refs/heads/master.zip', '/tmp/psycopg2.zip')
            shutil.unpack_archive('/tmp/psycopg2.zip' , '/tmp')
        os.symlink('/tmp/awslambda-psycopg2-master/psycopg2-3.8/', '/tmp/lambda_python_libs/python/psycopg2')


    def build_s3_buckets(self):
        print("Creating     - S3 bucket: "+shared_vars.app_s3_config_bucket_name)

        s3_config_bucket = s3.Bucket(self, "s3-configs",
            bucket_name = shared_vars.app_s3_config_bucket_name,
            removal_policy = core.RemovalPolicy.RETAIN,
            block_public_access = BlockPublicAccess.BLOCK_ALL,
            encryption= s3.BucketEncryption.S3_MANAGED,
            versioned = True)

        return s3_config_bucket


    def build_task(self):
        task_policy = {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Effect": "Allow",
                    "Action": [
                        "rds:CreateDBInstance",
                        "rds:ModifyDBInstance",
                        "rds:StopDBInstance",
                        "rds:StartDBInstance",
                        "rds:DeleteDBInstance",
                        "rds:DescribeDBInstances"
                    ],
                    "Resource": [
                        "arn:aws:rds:*:"+self.account+":db:*",
                        "arn:aws:rds:*:"+self.account+":og:*",
                        "arn:aws:rds:*:"+self.account+":pg:*",
                        "arn:aws:rds:*:"+self.account+":subgrp:*",
                        "arn:aws:rds:*:"+self.account+":secgrp:*"
                    ]
                },
                {
                    "Effect": "Allow",
                    "Action": "s3:*",
                    "Resource": [
                        "arn:aws:s3:::"+shared_vars.s3_source_bucket,
                        "arn:aws:s3:::"+shared_vars.s3_source_bucket+"/*",
                        "arn:aws:s3:::"+shared_vars.s3_destination_bucket,
                        "arn:aws:s3:::"+shared_vars.s3_destination_bucket+"/*",
                    ]
                }
            ]
        }

        task_role = Role(self, "task-role",
                            role_name=shared_vars.app_task_role_name,
                            assumed_by=_iam.ServicePrincipal("ecs-tasks.amazonaws.com"),
                            managed_policies=[_iam.ManagedPolicy.from_aws_managed_policy_name("service-role/AmazonECSTaskExecutionRolePolicy")],
                            inline_policies={'createRdsInstance': _iam.PolicyDocument.from_json(task_policy)}
                        )

        fargate_task_definition = ecs.FargateTaskDefinition(self, "TaskDef", 
            family=shared_vars.app_task_name,
            memory_limit_mib=shared_vars.app_task_mem_limit,
            cpu=(shared_vars.app_task_mem_limit / 2),
            #ephemeral_storage_gib=21,
            task_role=task_role, 
            #execution_role=
        )
        fargate_task_definition.add_container(shared_vars.app_task_container_name,
            image=ecs.ContainerImage.from_ecr_repository(ecr.Repository.from_repository_name(self, "scrub_repo", "blove_repo")),
            logging=ecs.LogDriver.aws_logs(stream_prefix=shared_vars.app_stack_name)
        )


    def build_lambda(self, core_obj_map, s3_config_bucket):
        print("Creating     - lambda role: "+shared_vars.app_lambda_role_name)

        vpc = core_obj_map['obj_vpc']
        #sec_group = core_obj_map['obj_sec_group']

        run_task_policy = { "Version": "2012-10-17",
                            "Statement": [
                                {   "Effect": "Allow",
                                    "Action": ["ecs:RunTask"],
                                    "Resource": [ "*" ]
                                },
                                {   "Effect": "Allow",
                                    "Action": [ "iam:PassRole" ],
                                    "Resource": [ "arn:aws:iam::"+self.account+":*" ]
                                }
                            ]
                        }
        lambda_role = Role(self, "lambda-role",
                            role_name=shared_vars.app_lambda_role_name,
                            assumed_by=_iam.ServicePrincipal("lambda.amazonaws.com"),
                            managed_policies=[
                                    _iam.ManagedPolicy.from_aws_managed_policy_name("service-role/AWSLambdaVPCAccessExecutionRole"),
                                    _iam.ManagedPolicy.from_aws_managed_policy_name("service-role/AWSLambdaBasicExecutionRole"),
                                    _iam.ManagedPolicy.from_aws_managed_policy_name("AmazonS3FullAccess")],
                            inline_policies={'runTask': _iam.PolicyDocument.from_json(run_task_policy)}
                          )
                            

        #self.create_dependency_pkg()
        # create a layer that holds the python dependencies so we don't have to upload them every time we change the lambda code
        lv = _lambda.LayerVersion(self, "lambda_python_libs",
                layer_version_name="libs",
                compatible_runtimes=[_lambda.Runtime.PYTHON_3_9],
                code=_lambda.Code.from_asset('lambdas/lambda_python_libs.zip')
                )

        #  yes, this creates an extra useless placeholder lambda with a new associated policy and role... https://github.com/aws/aws-cdk/issues/2781
        print("Creating     - lambda: "+shared_vars.app_lambda_name)
        _lambda.Function(self, "scrub_lambda",
                function_name=shared_vars.app_lambda_name,
                code=_lambda.Code.from_asset("lambdas/code/"),
                memory_size=1024,
                handler="controller.handler",
                runtime=_lambda.Runtime.PYTHON_3_9,
                timeout=core.Duration.seconds(60),
                role=lambda_role,
                vpc=vpc,
                layers=[lv],
                vpc_subnets=SubnetSelection(subnets=vpc.private_subnets),
                #security_groups=[sec_group],
                environment={"CONFIG_BUCKET":shared_vars.app_s3_config_bucket_name,
                            "CONFIG_DEFAULT_FILE":shared_vars.app_config_default_file,
                            "CONFIG_CUSTOM_FILE":shared_vars.app_config_custom_file,
                            "TASK_FAMILY":shared_vars.app_task_name,
                            "CONTAINER_NAME":shared_vars.app_task_container_name,
                            "SOURCE_BUCKET": shared_vars.s3_source_bucket,
                            "DESTINATION_BUCKET": shared_vars.s3_destination_bucket}
        )

