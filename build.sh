set -e

# build the docker image for the AWS task
(cd aws-cdk/tasks
docker build -t blove_repo . --progress=plain #--no-cache
docker image prune -f)

# push image to AWS ECR
aws ecr get-login-password --region us-west-2 | docker login --username AWS --password-stdin 247951156930.dkr.ecr.us-west-2.amazonaws.com
docker tag blove_repo:latest 247951156930.dkr.ecr.us-west-2.amazonaws.com/blove_repo:latest
docker push 247951156930.dkr.ecr.us-west-2.amazonaws.com/blove_repo:latest




