# Database Backup Scrubber #

## Summary ##
This application reads and updates a configuration file.\
Using that contents of that configuration file, this application will:
1. restore a database backup
2. remove/replace/redact sensitive information
3. generate a new, clean backup file
4. push that file to an S3 location where it can safely be consumed by the data team.

## Architecture ##

This application consists of 3 basic pieces:
1. Script to create AWS objects
2. A Python script that will be the Lambda code in AWS
3. A Python script that will be the ECS Task code in AWS

## Deployment Steps ##

### Prerequisites ###
- docker installed and running (Docker Desktop was used for development)

### Initial Deploy ###
1. Configure AWS credentials
```
    aws configure
```
2. Create AWS objects (from project root dir)
```
    cd aws-cdk
    cdk deploy
```
3. Build and push the Docker image (from project root dir)
```
    ./build.sh
```
## Config File ##
The scrubber config file must follow a specific format...

```
# these are required in the default config
redactChar: '#'
replaceStr: 'Lorem Ipsum'

# patterns are just variables that hold strings that you can reference while defining behavior for tables below
patterns:
  email: '.*@*\.com'
  ss: '[0-9]{3}-[0-9]{2}-[0-9]{4}'
  cc: '[0-9]{4}'

# if a column is specified with no other information its value will be hashed
tables:    # one only
    table1:    # one or many
        column1:    # one or many
            <action>:    # zero, one or many
                - <pattern>    # zero, one or many
            <action>:
                - <pattern>
                - <pattern>
    table2:
        column:
```
- `column`  _(required)_
- - If column has no corresponding action, the entire column value will be MD5 hashed
- `action`  _(optional)_
- - *remove*: removes any string that matches the pattern. If no pattern is specified then the entire column value is replaced with an empty string.
- - *redact*: replaces each character in each matched string with the character specified by _redactChar_ above.
- - *hash*:   replaces each occurrence of the string with its MD5 hashed value to retain uniqueness.
- - *replace*: replaces each occurence of the string with a new string specified by _replaceStr_ above.
- `pattern` _(optional)_ = any of the pattern names from the _patterns_ section at the top of the config file .
