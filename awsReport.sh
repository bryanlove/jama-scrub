#!/bin/bash

echo "- Subnets"
aws ec2 describe-subnets | jq -rc '.Subnets[] | (.Tags[]|select(.Key == "Name").Value)+" = "+.CidrBlock' | cut -d"/" -f3- | sort
echo ""

echo "- Elastic IPs"
aws ec2 describe-addresses | jq -rc '.Addresses[] | (.Tags[]|select(.Key == "Name").Value)+" = "+.PublicIp'
echo ""

exit
echo "- Attached Policies"
aws iam list-attached-group-policies --group-name developers | jq -r '.AttachedPolicies[].PolicyName'
echo ""

echo "- Inline Group Policies"
for x in $(aws iam list-group-policies --group-name developers | jq -r '.PolicyNames[]'); do
  aws iam get-group-policy --group-name Developers --policy-name $x
  echo ""
done